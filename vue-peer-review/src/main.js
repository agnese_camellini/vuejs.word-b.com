import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify';
import { config } from './config';
import { CollapsePlugin } from 'bootstrap-vue'
import VueCookies from 'vue-cookies'
import VueMeta from 'vue-meta'


Vue.use(VueMeta)
Vue.use(VueCookies)
Vue.use(CollapsePlugin)
Vue.config.productionTip = false
Vue.prototype.appConfig = config

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
