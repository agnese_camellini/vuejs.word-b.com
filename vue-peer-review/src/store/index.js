import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import VueAxios from 'vue-axios'
import { config } from '../config';
import router from '../router';

var sha256 = require('js-sha256');
Vue.use(Vuex)
Vue.use(VueAxios, axios)
var tmStamp = new Date();
var access_token = sha256(tmStamp.getTime().toString());
var token_confirm = sha256((10 * tmStamp.getTime()).toString())
var token_cng_pwd  = sha256((3 * tmStamp.getTime()).toString())
var hostName = window.location.hostname;
var hostPort = window.location.port;
var hostProtocol = window.location.protocol;
export default new Vuex.Store({
  strict: true,
  state: {
    activeAnonymDocumentInterface: false,
    isLoggedIn : false,
    activateRevisionInterface : false,
    processingLoginError : {
      "loginErrorKO": false,
      "loginErrorOK": false,
      "logoutErrorOK": false,
      "logoutErrorKO": false,
      "confirmationErrorKO": false,
      "confirmationErrorOK": false,
      "pwdErrorOK": false,
      "pwdErrorKO": false,
      "pwdConfirmOK":false,
      "pwdConfirmKO":false,
      "updateUserInfoOK": false,
      "updateUserInfoKO": false,
      "emailConfirmedKO": false,
      "goToInboxOK":false
    },
    userSingInForm :{
      "name": "",
      "username": "",
      "pwd": "",
      "email": "",
      "access_token": access_token,
      "token_confirm": token_confirm,
      "token_cng_pwd": token_cng_pwd,
      "flg_confirm": false
    },
    userInfoFom:{
      "id": "",
      "name": "",
      "username": "",
      "pwd": "",
      "email": "",
      "access_token": "",
      "token_confirm": "",
      "token_cng_pwd": "",
      "flg_confirm": true
    },
    confirmationBaseUrl: hostProtocol + "//" + hostName + ":" + hostPort,
    userLoginForm :{
        "username": "",
        "pwd": ""
      },
    docRevision : {
      "revision_id": undefined,
      "document_id": undefined
    },
    isRevisionLoaded : false,
    isProcessing: false,
    accountData: [],
    documentList : [],
    documentDetails : {},
    category : [],
    outline: [],
    revisions: [],
    keyList: [],
    nextLink: null,
    previousLink: null,
    documentKO: false,
    mimetypeOK:false,
  },

  actions: {
    resetMimeTypeError({commit}){
      commit("SET_MIMETYPE_ERROR", false);
    },
    setNewPwd({ commit,dispatch},{pwd, account}){
      commit('SET_PROCESSING', true);
      var user = {};
      user.id = account.id;
      user.name = account.name;
      user.username = account.username;
      user.pwd = sha256(pwd);
      user.email = account.email;
      user.access_token = account.access_token;
      user.token_confirm = account.token_confirm;
      user.token_cng_pwd = account.token_cng_pwd;
      user.flg_confirm = true;
      dispatch("setAccessToken", user.access_token)
      axios.put(config.$api_url + 'user/' + user.id+'/', user)
      .then((response) => {
        commit('SET_PROCESSING', false);
        if (response.data) {
          console.log("Update User Info process - Response: " + JSON.stringify(response.data));
          dispatch('pwdConfirm', {data :[response.data], bool: true});
          commit('SET_ACCOUNT', {val: false, data:response.data});
          console.log('setAccessToken: '+response.data.access_token)
          dispatch('setAccessToken', response.data.access_token);
        } else {
          dispatch('pwdConfirm', {dta:[], bool:false});
        }
      })
      .catch(function (error) {
          console.log(error);
      })
    },
    pwdConfirm({commit},{data, bool}){
      if(bool==true){
        commit('SET_ACCOUNT', true, data[0]);
      }
      commit('SET_PWD_CHANGE', bool);
    },
    updateUserInfoFom({commit}, form){
      commit('SET_NEW_USR_INFO', form);
    },
    userUpdate({commit, dispatch, state}){
      commit('SET_PROCESSING', true);
      var user = {};
      user.id = state.userInfoFom.id;
      user.name = state.userInfoFom.name;
      user.username = state.userInfoFom.username;
      user.pwd = sha256(state.userInfoFom.pwd);
      user.email = state.userInfoFom.email;
      user.access_token = state.userInfoFom.access_token;
      user.token_confirm = state.userInfoFom.token_confirm;
      user.token_cng_pwd = state.userInfoFom.token_cng_pwd;
      if(user.email!=state.accountData[0].email){
        user.flg_confirm = false;
        dispatch("sendConfirmationEmail", user);
      }else{
        user.flg_confirm = true;
      }
        axios.put(config.$api_url + 'user/' + user.id+'/', user)
        .then((response) => {
          if (response.data) {
            commit('SET_PROCESSING', false);
            console.log("Update User Info process - Response: " + JSON.stringify(response.data));
            if(response.data.flg_confirm!=false){
              commit('SET_ACCOUNT',{val: true, data:response.data});
              commit("SET_USERINFOUPDATE_SUCCESS", true);
            } else{
              commit('SET_EMAIL_CONFIRMATION', true);
              commit('GO_TO_INBOX', false);
              commit("SET_USERINFOUPDATE_SUCCESS", true);
            }
          } else {
            commit("SET_USERINFOUPDATE_SUCCESS", false);
          }
        })
        .catch(function (error) {
            console.log(error);
        })
    },
    takeAccountIntoUserInfoForm({commit}){
      commit('SET_USER_INFO_FORM');
      commit('RESET_DOCREVISION');
      commit('SET_ISREVISIONLOADED', false)
      commit('SET_ACTIVATE_REVISION_INTERFACE', false);
    },
    changeActivateRevisionInterface({commit}, bool){
      commit('SET_ACTIVATE_REVISION_INTERFACE', bool);
    },
    changeAnonymousDocumentState({commit}, bool){
      commit('SET_ACTIVATE_ANONYMOUS_DOCUMENT_INTERFACE', bool);
      console.log("changeAnonymousDocumentState"+ this.activeAnonymDocumentInterface);
    },
    insertDocument ({ commit, dispatch}, dataForm) {
      console.log("Upload process -------- Start ---------")
      console.log("Upload process - Form 1: " + JSON.stringify(dataForm))
      commit('SET_PROCESSING', true)
        var document = {
            name: dataForm.get('title'),
            user_id: dataForm.get('user')
        };
        axios.post(config.$api_url + 'document/', document)
        .then((response) => {
          dataForm.append('document', response.data.id)
          dispatch('insertRevison', dataForm)
          })
        .catch(function (error) {
            console.log(error);
        })
        },
    insertRevison ({commit, dispatch, state }, dataForm) {
      if (state.isProcessing == false){
        commit('SET_PROCESSING', true)
      }
      console.log("Upload process - Form 2: " + dataForm)
      commit('SET_DOCREVISION_DOCUMENT', dataForm.get('document'));
      console.log("Upload process - Get Document id: " + dataForm.get('document'))
      console.log("Upload process - Post Revision: " + JSON.stringify(state.docRevision))
      axios.post(config.$api_url + 'revisions/', state.docRevision)
      .then((response) => {
        console.log("Upload process - Get Revision id: " + response.data.id)
        dataForm.append('revision', response.data.id)
        commit('SET_DOCREVISION_REVISION', response.data.id)
        dispatch('uploadFile', dataForm);
        })
      .catch(function (error) {
          console.log(error);
      })
    },
    uploadRevision({commit}){
      commit('SET_ISREVISIONLOADED', false);
      commit('SET_ACTIVATE_REVISION_INTERFACE', true);
    },
    uploadFile ({dispatch}, dataForm) {
      console.log("Upload process - Form 3: " + dataForm)
      //console.log("Post Form: " + JSON.stringify(data))
        axios.post(config.$api_url + 'files/', dataForm)
        .then(() => {
          dispatch('generateCategoryByRevision', dataForm.get('revision'));
          })
    },
    generateCategoryByRevision ({commit, dispatch }, revisionId) {
      var req = "?revision_id=" + revisionId
        axios
          .get(config.$api_url + 'revision-category/generate_by_revision/'+ req)
          .then((response) => {
            if(response.data.bool!=undefined){
              commit('SET_PROCESSING', false);
              commit("SET_MIMETYPE_ERROR", true);
            }else {
              console.log("Upload process -------- End ---------");
              dispatch('loadCategory', revisionId);
              dispatch('loadDocumentDetails', revisionId);
              commit('SET_PROCESSING', false); 
              commit('SET_ACTIVATE_REVISION_INTERFACE', false);
  
            }
            })
          .catch(function (error) {
              console.log(error);
          })
    },
    obtainKeywordList({dispatch},id){
      var link = config.$api_url+'user-category/categories_by_user?user_id='+id+'&page=1&page_size=10'
      dispatch("obtainNextOrPreviousPageKeyword", link);
    },
    async obtainNextOrPreviousPageKeyword({commit}, link){
      axios
        .get(link)
        .then(response => {
            if(response.data.results.length>0){
              console.log("obtainNextOrPreviousPageKeyword:"+JSON.stringify(response.data.results));
                commit("SET_KEY_LIST", response.data.results);
                commit("SET_PREVIOUS_PAGE",response.data.previous);
                commit("SET_NEXT_PAGE", response.data.next);
            } 
        });
    },
    setCookiesForLogin(store, userData){
      Vue.$cookies.config('7d');
      Vue.$cookies.set('word-b.com-username', userData.username);
      Vue.$cookies.set('word-b.com-user-access', userData.access_token);
    },
    resetCookiesForLogin(){
      Vue.$cookies.remove('word-b.com-username');
      Vue.$cookies.remove('word-b.com-user-access');
    },
    userLogin ({commit, dispatch, state}) {
        commit('RESET_DOCREVISION');
        commit('SET_ISREVISIONLOADED', false)
        var user = {};
        user.username = state.userLoginForm.username;
        user.pwd = sha256(state.userLoginForm.pwd);
        axios.post(config.$api_url + 'user/list_login/', user)
        .then((response) => {
          if (response.data.length > 0) {
            if(response.data[0].flg_confirm == true){
              console.log("Response Login: " + JSON.stringify(response.data))
              commit('SET_LOGIN_SUCCESS', true);
              //state.accountData = response.data;
              commit('RESET_LOGIN_FORM');
              dispatch('resetCookiesForLogin');
              dispatch('setCookiesForLogin',response.data[0]);
              commit('SET_ACCOUNT', {val: true, data:response.data[0]});
              commit('SET_ACTIVATE_ANONYMOUS_DOCUMENT_INTERFACE', false);
              router.push({path: '/newDocument'});  
            } else {
              commit('SET_EMAIL_CONFIRMATION', true);
              commit('GO_TO_INBOX', false);
              commit('SET_ACCOUNT', {val: false, data:response.data[0]});
            }
          } else {
            commit('RESET_LOGIN_FORM')
            commit('SET_LOGIN_SUCCESS', false);
          }
          })
        .catch(function (error) {
            console.log(error);
        })
    },
    sendConfirmationEmail({commit}, user){
      axios.put(config.$api_url + "user/resend_activation_email/", user).then((response) => {
        if(response.data){
          commit('SET_EMAIL_CONFIRMATION', false);
          commit('GO_TO_INBOX', true);
        }
      });
    },
    resendConfirmationEmail({commit, state}){
      axios.put(config.$api_url + "user/resend_activation_email/", state.accountData[0]).then((response) => {
        if(response.data){
          commit('SET_EMAIL_CONFIRMATION', false);
          commit('GO_TO_INBOX', true);
        }
      });
    },
    userLogout ({commit, dispatch}) {
      commit('RESET_DOCREVISION');
      dispatch('resetCookiesForLogin');
      commit('RESET_ACCOUNT_DATA');
      commit('SET_LOGOUT_SUCCESS', true); 
      commit('SET_ISREVISIONLOADED', false);
      commit('SET_ACTIVATE_REVISION_INTERFACE', false);
      commit("SET_USERINFOUPDATE_SUCCESS", false);
    },
    SET_ACCOUNT_BY_COOKIES({state, commit, dispatch}, event){
      axios
        .get(config.$api_url+'user/list_username/?username=anonymous')
        .then(response => {
          var data = response.data;
          commit('SET_ACCOUNT_ANONYMOUS', data.user);
          dispatch('setAccessToken', state.accountData[0].access_token);
          axios
          .get(config.$api_url + 'user/list_access?access=' + Vue.$cookies.get('word-b.com-user-access'))
          .then((response) => {
            var data = response.data;
            if(data.bool== false){
              commit('SET_ACCOUNT_ANONYMOUS', data.user);
              dispatch('setAccessToken', data.user.access_token);
            } else {
              //state.accountData[0] = response.data;
              dispatch('setCookiesForLogin',data.user);
              commit('SET_ACCOUNT', {val: true, data:data.user});
              dispatch('setAccessToken', data.user.access_token);
              dispatch('setEventFlow', event);
            }
          })
          .catch(function (error) {
              console.log(error);
          })
        });
      
    },
    userSingIn ({commit/*,dispatch*/, state }, userFromComponent) {
      commit('SET_PROCESSING', true);
      commit('RESET_DOCREVISION');
      commit('SET_ISREVISIONLOADED', false);
      var user = {};
      user.name = userFromComponent.name;
      user.username = userFromComponent.username;
      user.pwd = sha256(userFromComponent.pwd);
      user.email = userFromComponent.email;
      user.access_token = state.userSingInForm.access_token;
      user.token_confirm = state.userSingInForm.token_confirm;
      user.token_cng_pwd = state.userSingInForm.token_cng_pwd;
      user.flg_confirm = false;
      var headers = {
        headers: { 'access':  state.accountData[0].access_token }
      }
      axios.post(config.$api_url + 'user/', user, headers)
        .then((response) => {
          if (response.data) {
            commit('SET_PROCESSING', false);
            commit('GO_TO_INBOX', true);
            commit("SET_CONFIRMATION_SUCCESS", true)
            router.push({path: '/loginForm'})
          } else {
            commit("SET_CONFIRMATION_SUCCESS", false)
          }
        })
        .catch(function (error) {
            console.log(error);
        })
    },
    accountConfirmation({ commit, dispatch}, data) {
      commit('SET_ACCOUNT', {val: false, data:data});
      dispatch('setAccessToken', data.access_token);
      axios
        .put(config.$api_url+'user/'+data.id+'/', data)
        .then((res) => {
          if (res.data) {
            commit("SET_CONFIRMATION_SUCCESS", true);
          } else {
            commit("SET_CONFIRMATION_SUCCESS", false);
          }
        });
    },
    async loadCategory ({ commit} , revisionId) {
      if(revisionId!=undefined){
        var req = "?revision_id=" + revisionId
        axios
          .get(config.$api_url + 'revision-category/category_by_revision/'+ req)
          .then((response) => {
            commit('SET_CATEGORY', response.data)
            })
          .catch(function (error) {
              console.log(error);
          });
      }
    },
    async loadIndex ({ commit} , revisionId) {
      if(revisionId!=undefined){
        axios
          .get(config.$api_url + 'revisions/'+ revisionId + '/')
          .then((response) => {
            commit('SET_OUTLINE', JSON.parse(response.data.outline));
            })
          .catch(function (error) {
              console.log(error);
          });
      }
    },
   async loadDocumentList ({ commit }, userId) {
      console.log("User id async: " + userId)
      if (userId > 0) {
          axios
            .get(config.$api_url + 'document/document_by_user/?user_id=' + userId)
            .then((response) => {
              commit('SET_DOCUMENT_LIST', response.data);
              commit('RESET_DOCREVISION');
              commit('SET_ISREVISIONLOADED', false);
              commit('SET_ACTIVATE_REVISION_INTERFACE', false);
              })
            .catch(function (error) {
                console.log(error);
            })
          }
    },
    async loadDocumentDetails ({ commit, dispatch}, revisionId) {
      console.log('Revision id:' + revisionId);
      axios
        .get(config.$api_url + 'revisions/' + revisionId+ '/')
        .then((response) => {
          dispatch('loadCategory', revisionId)
          dispatch('loadIndex', revisionId);
          commit('RESET_DOCREVISION');
          commit('SET_DOCREVISION_DOCUMENT', response.data.document_id);
          Vue.$cookies.set('word-b.com-document', response.data.document_id);
          commit('SET_DOCREVISION_REVISION', revisionId);
          commit('SET_ISREVISIONLOADED', true);
          commit('SET_DOCUMENT_DETAILS', response.data);
          dispatch('loadRevision');
          router.push({ path: '/documentDetails' });
          })
        .catch(function (error) {
            console.log('loadDocumentDetails error:'+error);
        })
    },
    async loadRevision ({commit, state}) {
          axios
            .get(config.$api_url + 'revisions/revision_by_document/?document_id=' + state.docRevision.document_id)
            .then((response) => {
              console.log("Revisioni lista: "+ JSON.stringify(response.data));
              commit('SET_REVISIONS', response.data);
              })
            .catch(function (error) {
                console.log(error);
            })
    },
    checkAccountData ({commit, dispatch, state}, event) {
      commit("RESET_USERINFOUPDATE_SUCCESS");
      commit("RESET_PWD_RESCUE_SUCCESS");
      commit("RESET_PWD_CHANGE");
      commit("RESET_SIGNIN_SUCCESS");
      commit('GO_TO_INBOX', false);
      dispatch("resetMimeTypeError");
      // commit("SET_DOCUMENT_KO", false);
      if (event == 'SingInForm' || event == 'PwdConfirm' || event == 'ForgotPwd' ) {
        dispatch("setAccessAnonymous");
        Vue.$cookies.remove('word-b.com-document');
      }
      if(event != 'LoginForm' && event != 'SingInForm'){
          if(event == "anonymousDocument"){
            commit("SET_DOCUMENT_KO", false);
            Vue.$cookies.remove('word-b.com-document');
          }
        if((state.accountData && state.accountData.length < 1) && !Vue.$cookies.isKey('word-b.com-user-access')){
          dispatch("setAccessAnonymous");
        }else if((state.accountData && state.accountData.length < 1) && Vue.$cookies.isKey('word-b.com-user-access')){
          dispatch('SET_ACCOUNT_BY_COOKIES', event);
        }
        if((state.accountData && state.accountData.length > 0) ){
          dispatch('setAccessToken', state.accountData[0].access_token);
          if(state.accountData[0].username!='anonymous'){
            dispatch('setCookiesForLogin',state.accountData[0]);
            dispatch('setEventFlow', event);  
          }
        }
      }
    },
    searchAnonymousDocument({state, commit, dispatch}, document){
      axios
        .get(config.$api_url+'user/list_username/?username=anonymous')
        .then(response => {
          var data = response.data;
          if(state.accountData.length== 0){
            commit('SET_ACCOUNT_ANONYMOUS', data.user); 
          }
          dispatch('setAccessToken', state.accountData[0].access_token); 
          axios
          .get(config.$api_url + 'document/anonimous_document?name='+document)
          .then((response) => {
            if (response.data.length > 0) {
                axios
                    .get(config.$api_url + 'revisions/revision_by_document?document_id='+response.data[0].id)
                    .then((response) => {
                        if (response.data.length > 0) {
                            dispatch('loadDocumentDetails',response.data[0].id);
                            commit('SET_ACTIVATE_ANONYMOUS_DOCUMENT_INTERFACE', true);
                            router.push({ path: '/documentDetails' })
                        } 
                    })
                    .catch(function (error) {
                        console.log(error);
                    })
            }else {
              commit("SET_DOCUMENT_KO", true);
            }
            })
          .catch(function (error) {
              console.log(error);
          })
        });
    },
    async setAccessAnonymous({state, commit, dispatch}){
      await axios
        .get(config.$api_url+'user/list_username/?username=anonymous')
        .then(response => {
          var data = response.data;
          commit('SET_ACCOUNT_ANONYMOUS', data.user);
          dispatch('setAccessToken', state.accountData[0].access_token);
        });
    },
    setAccessToken(state, id){
      axios.defaults.headers.common['access'] = id.toString();
    },
    setEventFlow({state, commit, dispatch},event){
      if(event == "DocumentList"){
        Vue.$cookies.remove('word-b.com-document');
        commit('SET_ACTIVATE_ANONYMOUS_DOCUMENT_INTERFACE', false);
        dispatch("loadDocumentList", state.accountData[0].id);
      } else if(event == 'Keywords'){
        Vue.$cookies.remove('word-b.com-document');
        dispatch('obtainKeywordList', state.accountData[0].id);
      } else if (event == 'NewDocument'){
        Vue.$cookies.remove('word-b.com-document');
        commit('SET_ISREVISIONLOADED', false)
        commit('SET_EMAIL_CONFIRMATION', false);
        commit('GO_TO_INBOX', false);
        if(state.accountData.length>0 && state.accountData[0].username != "anonymous"){
          commit('SET_ACTIVATE_ANONYMOUS_DOCUMENT_INTERFACE', false);
        } 
      }else if(event == 'DocumentDetails'){
        if(this.docDet==undefined ||this.docDet.document_id==undefined){
          if(Vue.$cookies.isKey('word-b.com-document')){
            axios
              .get(config.$api_url+'revisions/revision_by_document/?document_id='+Vue.$cookies.get('word-b.com-document'))
              .then(response => {
                if(response.data['detail'] != 'Not found.'){
                      dispatch('loadDocumentDetails',response.data[0].id);
                  }else{
                      router.push({path: '/NewDocument'})
                  }
              });
          } else{
            this.$router.push({ path: '/newDocument' });
          }
        }
      }
    },
    async getToken({commit, dispatch, state}){
      await axios
        .get(config.$api_url+'user/get_by_email/?email='+state.userSingInForm.email)
        .then(response => {
          var data = response.data;
          if(data.bool==true){
            data.user.pwd = '$$word-b-pwd-changing$$'
            dispatch('setAccessToken', data.user.access_token);
            axios
                .put(config.$api_url+'user/'+data.user.id+'/', data.user)
                .then((res) => {
                  if (res.data) {
                    commit("SET_PASSWORD_SUCCESS", true);
                  } else {
                    commit("SET_PASSWORD_SUCCESS", false);
                  }
                });
            } else{
              dispatch('setAccessToken', data.user.access_token);
              commit("SET_PASSWORD_SUCCESS", false);
            }
        });
    },
      SET_USER_NAME({commit}, input){
        commit("SET_USER_NAME", input)
      },
      SET_USER_PWD({commit}, input){
        commit("SET_USER_PWD", input)
      }
  },

  mutations: {
    SET_DOCUMENT_LIST (state, document) {
        state.documentList = document
    },
    SET_CATEGORY (state, category) {
      state.category = category
    },
    SET_OUTLINE (state, outline) {
      state.outline = outline;
    },
    RESET_ACCOUNT_DATA (state) {
      state.accountData = []
    },
    SET_ACCOUNT_ANONYMOUS (state, response) {
      state.accountData[0] = response;
    },
    SET_ACCOUNT(state, {val, data}){
      console.log("Commit Account data:" + JSON.stringify(data))
      state.accountData= [data];
      state.isLoggedIn = val
    },
    RESET_LOGIN_FORM(state){
      state.userLoginForm.username = ""
      state.userLoginForm.pwd = ""
    },
    SET_LOGIN_SUCCESS(state, bool){
      state.processingLoginError.loginErrorOK = bool;
      state.processingLoginError.loginErrorKO = !bool;
    },
    SET_LOGOUT_SUCCESS(state, bool){
      state.processingLoginError.loginErrorOK = !bool;
      state.processingLoginError.logoutErrorOK = bool;
      state.processingLoginError.logoutErrorKO = !bool;
      state.isLoggedIn = !bool;
    },
    SET_CONFIRMATION_SUCCESS(state, bool){
      state.processingLoginError.confirmationErrorOK = bool;
      state.processingLoginError.confirmationErrorKO = !bool;
    },
    SET_PASSWORD_SUCCESS(state, bool){
      state.processingLoginError.pwdErrorOK = bool;
      state.processingLoginError.pwdErrorKO = !bool;
    }
    ,
    SET_USER_NAME(state, input){
      state.userLoginForm.username = input;
    },
    SET_USER_PWD(state, input){
      state.userLoginForm.pwd = input;
    },
    SET_PROCESSING(state, isProcessing){
      state.isProcessing = isProcessing;
    },
    SET_DOCREVISION_DOCUMENT(state, document){
      state.docRevision.document_id = document;
    },
    SET_DOCREVISION_REVISION(state, revision){
      state.docRevision.revision_id = revision;
    },
    RESET_DOCREVISION(state){
      state.docRevision.revision_id = undefined;
      state.docRevision.document_id = undefined;
    },
    SET_DOCUMENT_DETAILS(state, revision){
      console.log('SET_DOCUMENT_DETAILS:'+JSON.stringify(revision));
      state.documentDetails = revision;
      console.log('SET_DOCUMENT_DETAILS2:'+JSON.stringify(state.documentDetails));
    },
    SET_REVISIONS(state, revisions){
      state.revisions = revisions;
    },
    SET_ISREVISIONLOADED(state, bool){
      console.log("before: " + JSON.stringify(state.isRevisionLoaded));
      state.isRevisionLoaded = bool;
      console.log("after:" + JSON.stringify(state.isRevisionLoaded));
    },
    SET_ACTIVATE_REVISION_INTERFACE(state, bool){
      state.activateRevisionInterface = bool;
    },
    SET_ACTIVATE_ANONYMOUS_DOCUMENT_INTERFACE(state, bool){
      state.activeAnonymDocumentInterface = bool;
    },
    SET_USER_INFO_FORM(state){
      state.userInfoFom.id = state.accountData[0].id;
      state.userInfoFom.name = state.accountData[0].name;
      state.userInfoFom.username = state.accountData[0].username;
      //state.userInfoFom.pwd = state.accountData[0].pwd;
      state.userInfoFom.email = state.accountData[0].email;
      state.userInfoFom.access_token = state.accountData[0].access_token;
      state.userInfoFom.token_confirm = state.accountData[0].token_confirm;
      state.userInfoFom.token_cng_pwd = state.accountData[0].token_cng_pwd;
      state.userInfoFom.flg_confirm = state.accountData[0].flg_confirm;
    },
    SET_USERINFOUPDATE_SUCCESS(state, bool){
      state.processingLoginError.updateUserInfoKO = !bool;
      state.processingLoginError.updateUserInfoOK = bool;
    },
    RESET_USERINFOUPDATE_SUCCESS(state){
      state.processingLoginError.updateUserInfoKO = false;
      state.processingLoginError.updateUserInfoKO = false;
    },
    SET_NEW_USR_INFO(state,form){
      state.userInfoFom.name = form.name;
      state.userInfoFom.username = form.username;
      state.userInfoFom.pwd = form.pwd;
      state.userInfoFom.email = form.email;
    },
    SET_KEY_LIST(state, list){
      state.keyList = list;
    },
    SET_PWD_CHANGE(state, bool){
      console.log("SET_PWD_CHANGE: "+ bool)
      state.processingLoginError.pwdConfirmOK=bool;
      state.processingLoginError.pwdConfirmKO=!bool;        
    },
    RESET_PWD_CHANGE(state){
      state.processingLoginError.pwdConfirmOK=false;
      state.processingLoginError.pwdConfirmKO=false;
    },
    SET_PREVIOUS_PAGE(state, link){
      state.previousLink = link;
    },
    SET_NEXT_PAGE(state, link){
      state.nextLink= link;
    },
    RESET_PWD_RESCUE_SUCCESS(state){
      state.processingLoginError.pwdErrorOK=false;
      state.processingLoginError.pwdErrorKO=false;
    },
    RESET_SIGNIN_SUCCESS(state){
      state.processingLoginError.confirmationErrorOK=false;
      state.processingLoginError.confirmationErrorKO=false;
    },
    SET_EMAIL_CONFIRMATION(state, bool){
      state.processingLoginError.emailConfirmedKO = bool;
    },
    GO_TO_INBOX(state, bool){
      state.processingLoginError.goToInboxOK = bool;
    },
    SET_DOCUMENT_KO(state, bool){
      this.documentKO = bool;
    },
    SET_MIMETYPE_ERROR(state, bool){
      this.mimetypeOK = bool;
    }
  }
})