let config;

config = {
  $api_url: "https://peerreview.word-b.com/api/v3/",
  $isLoggedIn : false

};

export { config }