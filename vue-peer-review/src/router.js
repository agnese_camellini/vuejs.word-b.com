import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import LoginForm from './views/LoginForm.vue'
import DocumentList from './views/DocumentList.vue'
import DocumentDetails from './views/DocumentDetails.vue'
import SingInForm from './views/SingInForm.vue'
import NewDocument from './views/NewDocument.vue'
import ForgotPwd from './views/ForgotPwd.vue'
import AccountConfirm from './views/AccountConfirm'
import PwdConfirm from './views/PwdConfirm'
import LoginOutPage from './views/LoginOutPage'
import AUserDocument from './views/AUserDocument'
import UserInfo from './views/UserInfo'
import Keywords from './views/Keywords'
import Terms from './views/Terms'
import About from './views/About'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      component: Home
    },
    {
      path: '/documentList',
      component: DocumentList
    },
    {
      path: '/documentDetails',
      component: DocumentDetails
    },
    {
      path: '/singInForm',
      component: SingInForm
    },
    {
      path: '/loginForm',
      component: LoginForm
    },
    {
      path: '/logOut',
      component: LoginOutPage
    },
    {
      path: '/newDocument',
      component: NewDocument
    },
    {
      path: '/forgotPwd',
      component: ForgotPwd
    },
    {
      path: '/account_confirmation',
      component: AccountConfirm
    },
    {
      path: '/pwd_confirmation',
      component: PwdConfirm
    },
    {
      path: '/a_user_document',
      component: AUserDocument
    },
    {
      path: '/UserInfo',
      component: UserInfo
    },
    {
      path: '/Keywords',
      component: Keywords
    },
    {
      path: '/About',
      component: About
    },
    {
      path: '/Terms',
      component: Terms
    }
  ]
})
