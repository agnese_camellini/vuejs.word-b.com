# README #

peerreviw.word-b.com

### What is this repository for? ###

This is the front end part of the application peerreviw.word-b.com
https://bitbucket.org/agnese_camellini/peerreview.word-b.com/src/master/

### How do I get set up? ###

Clone the repo as is shown in the page of the repository

cd "project directory"
npm install
npm run serve

### Contribution guidelines ###

The project is an idea of joy.indivia.net
The code is published under GPL version 2

### Who do I talk to? ###

To be incuded in the project write an email to:
agnese.camellini@gmail.com
